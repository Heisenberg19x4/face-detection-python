import datetime

import cv2
import os
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import queue
import threading
import time
import mtcnn
from deepface import DeepFace

from PIL import Image
model = mtcnn.mtcnn.MTCNN()

CAMERA_ADDRESS = ''

# *****************************************************************************
# ______________________________Find foto in directory___________________________
# *****************************************************************************
def get_cat(path_img,train_folder):
    result=DeepFace.find(img_path=path_img,db_path=train_folder,enforce_detection=False)
    result=result.values.tolist()
    return result

def DetectFace(image):
    faces = model.detect_faces(image)
    face_images = []

    if len(faces) != 0:
        for face in faces:
                # extract the bounding box from the requested face
            x1, y1, width, height = face["box"]
            x2, y2 = x1 + width, y1 + height

            # extract the face
            face_boundary = image[y1:y2, x1:x2]

            # resize pixels to the model size
            face_image = Image.fromarray(face_boundary)
            face_image = face_image.resize((128,128))
            face_images.append(np.array(face_image))
        print(len(faces))
        return face_images
    else:
        return []


def Shoot(*args):
    video = cv2.VideoCapture(CAMERA_ADDRESS)
    while (1):
        print("Im alive")
        ret, frame = video.read()
        if ret:
            frame = cv2.resize(frame, (1920, 1080))
            # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = DetectFace(frame)
            # print(faces)
            if len(faces) != 0:
                for face in faces:
                    cv2.imwrite(f"test.png", face)
                    cat = get_cat("test.png",
                                  r"./ImagePersonForLearning/prepared")
                    print(cat)
                    if len(cat) != 0:
                        if cat[0][1] > 0.1:
                            cv2.imwrite(f"./person/{cat[0][0]}_{datetime.now()}.png", face)
        else:
            continue

Shoot()
