import cv2
import os
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import mtcnn
from datetime import datetime
from PreparePhoto import *
import requests
import json
from minio import Minio
from dotenv import load_dotenv
from deepface import DeepFace
import queue
import threading

# *****************************************************************************
# ______________________________GLOBAL VARIABLES TREAD____________________________
# *****************************************************************************
PATHFILE="./"
queueLock = threading.Lock()  # Global parameter bloc/unlock all thread in our ThreadPool
KEYQueue = queue.Queue()  # global queue KEY image generate FaceDetection
exitSignal = 0  # make exitSignal=1 if you want stop all thread

ListCamera=[["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","0","progr"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","1","fabricEnter"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","2","medTech"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","3","fabricSRV1"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","4","mainOffice"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","5","fabricSRV2"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554", "6","fabricWorkSpace"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554", "8","fabric"],
            ["rtsp://view:`12345@THE IP ADDRESS OF YOUR CAMERA:554","9","officeDron"]]
# *****************************************************************************
# _________________________include MINIO_____________________
# *****************************************************************************
load_dotenv() # add env


LOCAL_FILE_PATH = os.environ.get("LOCAL_FILE_PATH")
ACCESS_KEY = os.environ.get("ACCESS_KEY")
SECRET_KEY = os.environ.get("SECRET_KEY")
MINIO_API_HOST = "minio:9000"
MINIO_CLIENT = Minio("minio:9000", access_key=ACCESS_KEY, secret_key=SECRET_KEY, secure=False)

def MinIOSaveImage(PATHImageForServer):
    print("in MinIOSaveImage",PATHImageForServer)
    found = MINIO_CLIENT.bucket_exists("faceid")
    if not found:
        MINIO_CLIENT.make_bucket("faceid")
    else:
        print("Bucket already exists")
    PATHImage=f"image{datetime.now()}.png"
    PATHImageWhoCome="ImagePersonWhoCome/"+PATHImageForServer
    MINIO_CLIENT.fput_object("faceid",PATHImage ,PATHImageWhoCome)
    print("It is successfully uploaded to bucket")
    return PATHImage
# *****************************************************************************
# _________________________sending the event to the server_____________________
# *****************************************************************************
URLJSON =  os.environ.get("URLJSON")
PATH_TO_IMAGE = "ImagePersonForLearning"
model = mtcnn.mtcnn.MTCNN()
def PeopleHandler(verification,PATHImageMinio,param,PATHimage=None ):
    # we get the status and path to the photo of a new person
#    param=json.dumps(param)
    try:
    # __________________If the person is identified:__________________
        if (verification==True):
            CountPeople=0
        # 1. We are already looking for " people : " in the file with this path > 1, then we do not send anything
            with open("OUTPUTVerifyTrue.txt", "r") as file:
                for line in file:
                    if PATHimage in line:
                        CountPeople+=1
            # 2. If " people : " == 0 , then we write to the file and send a json request
            if (CountPeople==0):
                with open("OUTPUTVerifyTrue.txt", "a") as fileWrite:
                    fileWrite.write("date: "+f"{datetime.now()}"+" people: "+f"{PATHimage}"+" photo: "+PATHImageMinio+"\n")
                resp=requests.post(URLJSON,json=param)

        # __________________If the person is not identified:__________________
        else:
        # 1. I am sending json the person is not recognized
            resp = requests.post(URLJSON, json=param)
    except:
        print("json error")


# *****************************************************************************
# ______________________________DETECTED FACE___________________________
# *****************************************************************************
model = mtcnn.mtcnn.MTCNN()
def DetectFace(image):
    faces = model.detect_faces(image) # detected face with MTCNN - neural networks | work with RGB
    face_images = []

    if len(faces) != 0:
        for face in faces:
                # extract the bounding box from the requested face
            x1, y1, width, height = face["box"]
            x2, y2 = x1 + width, y1 + height

            # extract the face
            face_boundary = image[y1:y2, x1:x2]

            # resize pixels to the model size
            face_image = Image.fromarray(face_boundary)
            face_image = face_image.resize((720,1080))
            face_images.append(np.array(face_image))
        print(len(faces))
        return face_images
    else:
        return []
# *****************************************************************************
# ______________________________SETTING FRAME___________________________
# *****************************************************************************
def prepare_img_in_stream(img_array):
    img = Image.fromarray(img_array)
    img.thumbnail((1920, 1080), Image.ANTIALIAS)
    img = np.array(img)
    return img

# *****************************************************************************
# ______________________________Find foto in directory___________________________
# *****************************************************************************
def get_cat(path_img,train_folder):
    result=DeepFace.find(img_path=path_img,db_path=train_folder,enforce_detection=False)
    result=result.values.tolist()
    return result
# *****************************************************************************
# ______________________________FaceDetectionCamera____________________________
# *****************************************************************************

def FaceDetectionCamera(Camera):
    # Create an object to read
    # from camera
    # video = cv2.VideoCapture("")

    print(f"{Camera} live")
    video = cv2.VideoCapture(Camera[0])

    if not video.isOpened():
        print("Error reading video file")
   
    while(1):
        ret, frame = video.read()
        if ret:
            #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) # convert in RGB
            frame = cv2.resize(frame,(1920,1080))
           # cv2.imwrite(f"ImagePersonWhoCome/test{count}.png",frame)
            faces = DetectFace(frame)
           
            if len(faces) !=0:
                for face in faces:
                    cv2.imwrite(f"ImagePersonWhoCome/test{Camera[1]}.png",face) # save image
                    cat = get_cat(f"ImagePersonWhoCome/test{Camera[1]}.png",r"ImagePersonForLearning/prepared") #FIND PERSON
                    #save minio
                    PATHImage=MinIOSaveImage(f"test{Camera[1]}.png") #return path save minio image
                    print(cat)
                    if len(cat)!=0:
                        if cat[0][1]<0.25:
                            # if find person
                           # cv2.imwrite(f"ReverseImagePersonWhoCome/image{Camera[1]}.png",face)
                            verification = True
                            param = {"verification": "True",
                                     "date": f"{datetime.now()}",
                                     "people": f"{cat[0][0]}",
                                     "photo": PATHImage,
                                     "place": Camera[2]
                                     }
                            PeopleHandler(verification, PATHImage, param, cat[0][0])

                        else:
                            with open("OUTPUTVerifyFalse.txt", "a") as file:
                                file.write("date: " + f"{datetime.now()}" + " people: " + "verification failed" + " photo: " + PATHImage + "\n")

                            verification = False

                            param = {"verification": "False",
                                     "date": f"{datetime.now()}",
                                     "people": "False",
                                     "photo": PATHImage
                                     }

                            PeopleHandler(verification, PATHImage, param)

        else:
            continue


# *****************************************************************************
# _____________________________THREAD CLASS____________________________________
# *****************************************************************************
# defining our own class implementation of threads
class ThreadForCalc(threading.Thread):


    def __init__(self, threadID, name, target,Camera):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.target = target
        self.Camera=Camera

    def run(self):
        print("Start " + self.name)
        #queueLock.release()  # removes the lock
        self.target(self.Camera)
        #queueLock.acquire()  # sets a lock
        print("Exit " + self.name)

'''
    def exitTread(self, ExitSIgnal):
        if (ExitSIgnal==1):
            queueLock.acquire()  # sets a lock
            self.join()
'''


# *****************************************************************************
# ________________________START FaceDetection______________________
# *****************************************************************************


if __name__ == '__main__':
#    Preparephoto()
   # print(len(ListCamera))
    ValueThread = len(ListCamera)   # setting the number of threads
    threadList = [f"Thread-{i}" for i in range(ValueThread)]  # setting thread names

    threads = []  # list for start thread
    threadID = 0
    # Create new threads
    for tName in threadList:
        thread = ThreadForCalc(threadID, tName, FaceDetectionCamera,ListCamera[threadID])
        thread.start()
        threads.append(thread)
        threadID += 1
        print("TREAD ", tName)

    for t in threads:
        t.join()
    # queueLock.acquire()  # sets a lock
