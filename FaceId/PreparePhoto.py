import cv2
import mtcnn
import numpy as np
from PIL import Image
import os

model = mtcnn.mtcnn.MTCNN()

def DetectFace(image):

    faces = model.detect_faces(image)
    face_images = []

    if len(faces) != 0:
        for face in faces:
                # extract the bounding box from the requested face
            x1, y1, width, height = face["box"]
            x2, y2 = x1 + width, y1 + height

            # extract the face
            face_boundary = image[y1:y2, x1:x2]

            # resize pixels to the model size
            face_image = Image.fromarray(face_boundary)
            face_image = face_image.resize((1080,1080)) #  face_image = face_image.resize((128,128))
            face_images.append(np.array(face_image))
        print(len(faces))
        return face_images
    else:
        return []


def Preparephoto():
    base =r"ImagePersonForLearning/newperson"
    dir_t = os.listdir(r"ImagePersonForLearning/newperson")
    for file in dir_t:
        file_to_read = os.path.join(base, file)
        print(file_to_read,os.path.isdir(file_to_read))
        if os.path.isdir(file_to_read) == False:
            second_base = os.path.join(base,"prepared")
            file_to_save = os.path.join(second_base,file)
            img = cv2.imread(file_to_read)
            faces = DetectFace(img)
            for face in faces:
                print(file_to_save)
                cv2.imwrite(file_to_save,face)